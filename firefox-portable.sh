#!/bin/bash
#
# This script allows you to install or update a portable version of the newest firefox without touching your standard
# firefox installation.
#
# See firefox-portable.sh -h oder firefox-portable.sh --help for more information
#

function install {
    # create installation folder if already there
    if ! [ -d $installpath ]; then
        mkdir $installpath
    fi

    geturl

    # install firefox portable
    wget $url -O $installpath/$downloadfile 
    echo "Extract firefox from archive..."
    tar -xf $installpath/$downloadfile -C $installpath
    rm $installpath/$downloadfile
    
    # create profile folder
    mkdir $profilepath
    
    # creater starter
    echo "#!/bin/sh" > $installpath/starter.sh
    echo '"'$installpath'/firefox/firefox" -no-remote -profile "'$profilepath'"' >> $installpath/starter.sh
    chmod +x $installpath/starter.sh

    exit 0
}

function update {
    # define default folders if not given by user
    [ -z "$installpath" ] && installpath="$(pwd)/"
    
    # check if firefox is installed 
    if ! [ -d "$installpath/firefox" ]; then
        echo "Could not found firefox in this directory. Sure you already installed it there?"
        echo "The given directory was $installpath"
        exit 0
    fi
    
    # check if already on newest available version
    installed_version="$(grep -o -m 1 '[0-9.]\+[0-9]' ~/firefox-portable/firefox/application.ini)"
    latest_version="$(curl -fsI 'https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US' | grep -o 'firefox-[0-9.]\+[0-9]' | grep -o '[0-9.]\+[0-9]')"

    if [[ ( "$installed_version" = "$latest_version" ) ]] ; then
        echo "You already have the newest available version ($latest_version) installed."
        echo "exit..."
        exit
    fi
    
    # backuo old files if something goes wrong. If there is an old backup, delete it.
    if [ -d "./firefox_old" ]; then
        rm -rf ./firefox_old
        mv ./firefox ./firefox_old
    else
        mv ./firefox ./firefox_old
    fi

    geturl
    
    wget $url -O $downloadfile
    tar -xf $downloadfile
    rm ./$downloadfile
    exit 0
}

function geturl() {
    case $version in
        normal ) url="https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=de";;
        beta ) url="https://download.mozilla.org/?product=firefox-beta-latest-ssl&os=linux64&lang=de";;
        nightly ) url="https://download.mozilla.org/?product=firefox-nightly-latest-l10n-ssl&os=linux64&lang=de";;
        developer ) url="https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=de";;
        *) echo "Unknown parameter for Option -v passed. Exit"; exit 0;;
    esac
}

function show_version() {
    installed_version="$(grep -o -m 1 '[0-9.]\+[0-9]' ~/firefox-portable/firefox/application.ini)"
    latest_version="$(curl -fsI 'https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US' | grep -o 'firefox-[0-9.]\+[0-9]' | grep -o '[0-9.]\+[0-9]')"

    echo "Installed version:        $installed_version "
    echo "Latest available version: $latest_version "
    
    exit 0
}

function help {
   # Display Help
   echo ""
   echo "This script allows you to install or update a portable version of the"
   echo "newest firefox without touching your standard firefox installation."
   echo ""
   echo "After the installation you can run the portable version of firefox"
   echo "by running the created starter.sh file."
   echo ""
   echo "Syntax: bash firefox-portable.sh [-i|u|p|v|h]"
   echo ""
   echo "-i   --install   Install portable firefox"
   echo ""
   echo "-u   --update    Update portable firefox to the newest version."
   echo ""
   echo "-p   --path      Define a specific path. This can be used for" 
   echo "                 installations and updates. If no path is given"
   echo "                 the path of this file is used for the installation"
   echo "                 or update"
   echo ""
   echo "-b   --build     With the argument you can define which build"
   echo "                 of firefox you want to install."
   echo "                 [normal|beta|nigthly|developer]"
   echo "                 Example: "
   echo "                    ./firefox-portable.sh -b beta"
   echo ""
   echo "-v  --version    Show the installed and the newest available"
   echo "		  version of Firefox."
   echo ""
   echo "-h   --help      Print this help and exit."
   echo ""
   exit 0
}

# set normal firefox as default version
version="normal"

downloadfile="firefox.tar.bz2"

# scan user inputs
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -i|--install) install="1" ;;
        -u|--update) update="1"  ;;
        -p|--path) installpath="$2" && profilepath="$2/profile"; shift ;;
        -b|--build) version="$2"; shift ;;
        -h|--help) help  ;;
	-v|--version) show_version; shift ;;
	*) echo "Unknown parameter passed: $1" ;;
    esac
    shift
done

# set default folders if not given by user
[ -z "$installpath" ] && installpath="$(pwd)/"
[ -z "$profilepath" ]  && profilepath="$(pwd)/profile"

# install or update
[ "$install" == "1" ]  && install
[ "$update" == "1" ]  && update
    
echo "No parameter passed. Run bash firefox-portable.sh -h for more information"

