# firefox-portable

## What does it do?
This script allows you to install or update a portable version of the newest firefox without touching your standard firefox installation.

You can specify the version to install (normal/beta/nightly/developer) as well as the installation path.
See `firefox-portable.sh -h` or `firefox-portable.sh --help` for more information.
